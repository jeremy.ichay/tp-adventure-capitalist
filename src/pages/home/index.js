import React from 'react';
import { connect } from 'react-redux';
import { Container, Row } from 'react-bootstrap';
import Page from '..';
import MoneyBar from '../../components/money-bar';
import Product from '../../components/product';
import UnlockBar from '../../components/unlock-bar';

const Home = ({ products }) => (
  <Page>
    <Container>
      <Row className="mb-3">
        <MoneyBar />
      </Row>
      <Row className="no-gutters">
        {products
          .filter((item) => item.unlocked)
          .map((item) => <Product item={item} />)}
      </Row>
      <UnlockBar />
    </Container>
  </Page>
);

const injectStateToProps = (state) => ({
  products: state.appReducer.products
});

export default connect(injectStateToProps)(Home);
