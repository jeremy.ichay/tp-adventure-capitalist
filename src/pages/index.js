import React from 'react';

const Page = ({ children }) => (
  <main className="container">
    {children}
  </main>
);

export default Page;
