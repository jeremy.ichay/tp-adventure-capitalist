export const productAction = {
  UNLOCK_PRODUCT: 'UNLOCK_PRODUCT'
};

export const unlockProduct = (index) => ({
  type: productAction.UNLOCK_PRODUCT,
  index
});
