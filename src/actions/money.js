export const moneyAction = {
  ADD_MONEY: 'ADD_MONEY',
  REMOVE_MONEY: 'REMOVE_MONEY'
};

export const addMoney = (money) => ({
  type: moneyAction.ADD_MONEY,
  money
});

export const removeMoney = (money) => ({
  type: moneyAction.REMOVE_MONEY,
  money
});
