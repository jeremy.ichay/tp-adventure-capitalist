import React, { useState } from 'react';
import { connect } from 'react-redux';
import store from '../../store';
import { unlockProduct } from '../../actions/product';
import { removeMoney } from '../../actions/money';

const UnlockBar = ({ products, money }) => {
  const getIndexNextProductToUnlock = () => (
    products.findIndex((item) => !item.unlocked)
  );

  const [currentProductIndex, setCurrentProductIndex] = useState(getIndexNextProductToUnlock);
  const [showButton, setShowButton] = useState(true);

  const handleBuyProduct = () => {
    if (currentProductIndex !== -1) {
      const price = products[currentProductIndex].unlockprice;
      if (money >= price) {
        store.dispatch(removeMoney(price));
        store.dispatch(unlockProduct(currentProductIndex));
        const nextIndex = getIndexNextProductToUnlock();
        if (nextIndex !== -1) {
          setCurrentProductIndex(nextIndex);
        } else {
          setShowButton(false);
        }
      }
    }
  };

  return (
    <div>
      {showButton
        && (
          <button onClick={handleBuyProduct} type="button" className="btn btn-warning">
            {`Get next GPU for ${products[currentProductIndex].unlockprice} ₿`}
          </button>
        )}
    </div>
  );
};

const injectStateToProps = (state) => ({
  products: state.appReducer.products,
  money: state.appReducer.money
});

export default connect(injectStateToProps)(UnlockBar);
