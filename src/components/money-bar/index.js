import React from 'react';
import { connect } from 'react-redux';

const MoneyBar = ({ money }) => (
  <div className="card h-md-100">
    <div className="card-header pb-0">
      <b>Earned BITCOIN ₿₿₿</b>
    </div>
    <div className="card-body d-flex align-items-end">
      <div className="row flex-grow-1">
        <div className="col">
          <div className="fs-4 font-weight-normal text-sans-serif text-700 line-height-1 mb-1">
            {money}
          </div>
        </div>
      </div>
    </div>
  </div>
);

const injectStateToProps = (state) => ({
  money: state.appReducer.money
});

export default connect(injectStateToProps)(MoneyBar);
