import React, { useEffect, useState, useRef } from 'react';
import { connect } from 'react-redux';
import store from '../../store';
import { addMoney, removeMoney } from '../../actions/money';
import gpu from '../../assets/images/gpu.png';
import './index.css';

const Product = ({ item, money }) => {
  const
    {
      name,
      earning,
      upgrade,
      autoMiner
    } = item;

  const [localUpgrade] = useState(upgrade);
  const [currentUpgradeIndex, setCurrentUpgradeIndex] = useState(0);
  const [showUpgrade, setShowUpgrade] = useState(true);
  const [autoMinerState, setAutoMinerState] = useState(false);
  const [showAutoMinerButton, setShowAutoMinerButton] = useState(true);

  function useInterval(callback, delay) {
    const savedCallback = useRef();
    useEffect(() => {
      savedCallback.current = callback;
    }, [callback]);
    useEffect(() => {
      function tick() {
        savedCallback.current();
      }
      if (delay !== null) {
        const id = setInterval(tick, delay);
        return () => clearInterval(id);
      }
      return [];
    }, [delay]);
  }

  const earnClick = () => {
    const currentUpgradeAdd = localUpgrade[currentUpgradeIndex].earningAdd;
    store.dispatch(addMoney(earning + currentUpgradeAdd));
  };

  useInterval(() => {
    if (autoMinerState) {
      earnClick();
    }
  }, 1000);

  const handleUpgradeClick = () => {
    if (currentUpgradeIndex < localUpgrade.length - 1
      && money >= localUpgrade[currentUpgradeIndex + 1].cost) {
      store.dispatch(removeMoney(localUpgrade[currentUpgradeIndex + 1].cost));
      if (currentUpgradeIndex + 1 === localUpgrade.length - 1) {
        setShowUpgrade(false);
      }
      setCurrentUpgradeIndex(currentUpgradeIndex + 1);
    }
  };

  const handleMinerUnlockClick = () => {
    store.dispatch(addMoney(autoMiner.cost));
    setShowAutoMinerButton(false);
    setAutoMinerState(true);
  };

  const handleEarnClick = () => {
    earnClick();
  };

  return (
    <div className="col-md-12 col-xxl-3 mb-3 pr-md-2">
      <div className="card">
        <div className="card-header pb-0">
          <b>{`${name} ${localUpgrade[currentUpgradeIndex].name}`}</b>
        </div>
        <div className="card-body d-flex align-items-end">
          <div className="row flex-grow-1">
            <div className="col">
              <div className="fs-4 font-weight-bold text-sans-serif text-700 line-height-1 mb-1">
                {`${earning + localUpgrade[currentUpgradeIndex].earningAdd}₿ / click`}
              </div>
            </div>
            <div className="col">
              <input type="image" className="gpu-icon" alt={gpu} src={gpu} onClick={handleEarnClick} />
            </div>
            <div className="col">
              {showUpgrade
                && (
                  <button type="button" className="btn btn-success upgrade-btn" onClick={handleUpgradeClick}>
                    {`Upgrade GPU for ${localUpgrade[currentUpgradeIndex + 1].cost}₿`}
                  </button>
                )}
              {showAutoMinerButton
                && (
                  <button type="button" className="btn btn-success upgrade-btn" onClick={handleMinerUnlockClick}>
                    {`Auto miner for ${autoMiner.cost}₿`}
                  </button>
                )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const injectStateToProps = (state) => ({
  money: state.appReducer.money
});

export default connect(injectStateToProps)(Product);
