import { combineReducers } from 'redux';

import appReducer from './data';

export default combineReducers({
  appReducer
});
