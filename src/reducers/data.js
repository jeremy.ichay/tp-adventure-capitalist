import { moneyAction } from '../actions/money';
import { productAction } from '../actions/product';

const initialState = {
  products: [{
    unlocked: true,
    unlockprice: 0,
    name: 'GTX',
    earning: 1,
    autoMiner: {
      cost: 10000
    },
    upgrade: [{
      cost: 0,
      earningAdd: 0,
      name: '1060'
    }, {
      cost: 1000,
      earningAdd: 2,
      name: '1070'
    }, {
      cost: 3000,
      earningAdd: 4,
      name: '1080'
    }, {
      cost: 5000,
      earningAdd: 6,
      name: '1080 OC'
    }]
  }, {
    unlocked: false,
    unlockprice: 10000,
    name: 'RTX',
    earning: 60,
    autoMiner: {
      cost: 100000
    },
    upgrade: [{
      cost: 0,
      earningAdd: 0,
      name: '2060'
    }, {
      cost: 10000,
      earningAdd: 30,
      name: '2070'
    }, {
      cost: 17000,
      earningAdd: 60,
      name: '2080'
    }, {
      cost: 24000,
      earningAdd: 120,
      name: '2080 OC'
    }]
  }, {
    unlocked: false,
    unlockprice: 100000,
    name: 'RTX',
    earning: 1000,
    autoMiner: {
      cost: 1000000
    },
    upgrade: [{
      cost: 0,
      earningAdd: 0,
      name: '3060'
    }, {
      cost: 100000,
      earningAdd: 2000,
      name: '3070'
    }, {
      cost: 120000,
      earningAdd: 4000,
      name: '3080'
    }, {
      cost: 200000,
      earningAdd: 6000,
      name: '3090'
    }]
  }],
  money: 10000001
};

const unlockProduct = (state, action) => {
  const { index } = action;
  const stateUpdate = [...state.products];
  stateUpdate[index].unlocked = true;

  return {
    money: state.money,
    products: stateUpdate
  };
};

const addMoney = (state, action) => ({
  products: state.products,
  money: state.money + action.money
});

const removeMoney = (state, action) => ({
  products: state.products,
  money: state.money - action.money
});

const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case moneyAction.ADD_MONEY:
      return addMoney(state, action);
    case moneyAction.REMOVE_MONEY:
      return removeMoney(state, action);
    case productAction.UNLOCK_PRODUCT:
      return unlockProduct(state, action);
    default:
      return state;
  }
};

export default appReducer;
